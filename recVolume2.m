function [recVol,xl,yl,zl] = recVolume2( tfilename, ufilename,dx,align)
%this program is used to reconstruct 3D volume images based on tracker data only.
%Input arguments are the tracker filename, ultrasound filename and the
%final resolution desired.
%recVolume2 returns the limits (min and Max) of the volume.

%set the resolution
if nargin  < 3, dx = 0.0005;end
if nargin < 4, align = 0; end

dy = dx;	%make resolution uniform in all directions
dz = dx;	

d1=0.038;             	%width of L14-8 array (in meter)
offsetx=-0.015;     	%boresight set with probe vertical and || to Y tracker axis
offsety=0.0135          %boresight set with probe vertical and || to X tracker axis
offsetz=0.040;

%read all the frames and coordinates
[f0,t0,tt,ut] = calc_times(tfilename,ufilename);
hout = syncFrameTimes( t0, f0, tfilename, ufilename);

if align == 1
    [stack,XX,YY]=alignStack(hout.data);
    hout.data = stack;
    Y0=hout.Y(1);
    hout.Y = Y0 + cumsum(XX) * dx/1000;
end



%determine the overall extent of the volume to be reconstructed (in tracker coordinates)
%boresight set with probe vertical and || to X tracker axis
xmin = min(hout.X) - d1/2;
ymin = min(hout.Y) + offsety;
zmin = min(hout.Z) + offsetz;
xmax = max(hout.X) + d1/2;
ymax = max(hout.Y) + offsety;
zmax = max(hout.Z) + offsetz;

%adjust to accomodate the US image
[ny,nx,nf] = size(hout.data);
d2 = ny*d1/nx;          %adjust the depth to the FOV used.
res = hout.res/1000
zmin = zmin + 0.005;     %try to eliminate the skin=line
zmax = zmax + (res * ny); %Add a bit to accomodate the US image
xmin = xmin - (res * nx/2)-.010;
xmax = xmax + (res * nx/2)+.010;
ymin = ymin -.010;
ymax = ymax +.010;

NX = ceil((xmax - xmin)/dx);
NY = ceil((ymax - ymin)/dy);
NZ = ceil((zmax - zmin)/dz);

%setup a 3D volume for reconstructed output
recVol = zeros(NY,NX,NZ);	%array to accumulate image data.
recHit = 10000*ones(NY,NX,NZ);	%array to keep track of the number of times data is placed in this element

% downsample the raw image data
sf = res/dx
[ny,nx,nf] = size(hout.data)
for i=1:nf
	scaledImg = imresize(hout.data(:,:,i),sf);
	if i==1
		[sy,sx] = size(scaledImg);
		rawVol = zeros(sy,sx,nf);
	end
	rawVol(:,:,i) = max(0,scaledImg);
end

%convert angles to radians
tmp = hout.yaw;   hout.yaw = tmp * pi/180;
tmp = hout.pitch; hout.pitch = tmp * pi/180;
tmp = hout.roll;  hout.roll = tmp * pi/180;


%Now ready to place the images into recVol
for i=1:nf
	[ Xrt, Yrt, Zrt ] = calc_planeCoords2( hout.X(i), hout.Y(i), hout.Z(i), ...
		hout.yaw(i), hout.pitch(i), hout.roll(i), offsety, offsetz, d1, d2,sx,sy);
%        0, 0, 0, offsety, offsetz, d1, d2,sx,sy);

	%convert the planar coordinates to indices
	Xind = round((Xrt - xmin)/dx);
	Yind = round((Yrt - ymin)/dy);
	Zind = round((Zrt - zmin)/dz);

	%test the range of the indices
    maxXind = max(max(Xind));
    maxYind = max(max(Yind));
    maxZind = max(max(Zind));
    minXind = min(min(Xind));
    minYind = min(min(Yind));
    minZind = min(min(Zind));
    
    for iy = 1:sy
		for ix = 1: sx
            xx = Xind(iy,ix);
            yy = Yind(iy,ix);
            zz = Zind(iy,ix);
            
            if (xx > 0) && (xx <= NX)
                if (yy > 0) && (yy <= NY)
                    if (zz > 0) && (zz <= NZ)
                        recVol(yy,xx,zz) = 100;%rawVol(iy,ix,i);
                        q=recHit(yy,xx,zz);
                        if q == 10000; q=0;end; %first time this voxel used
                        recHit(yy,xx,zz) = q+1;
                    end
                end
            end
        end
    end
end


%normalize
A = recVol ./ recHit;

%do some hole filling
recVol = holefillyint(A);
%recVol = A;
xl = [xmin,xmax];
yl = [ymin,ymax];
zl = [zmin,zmax];




