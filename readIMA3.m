function h = readIMA3(pathname,name,fl,infoFile,displayflag)
%
%
%

if nargin < 3, fl=0;end
if nargin < 4, infoFile = 'info.csv'; end
if nargin < 5, displayflag =1; end

info = readInfo2(pathname,name,infoFile);

h.frames = info.nframes;    %    int frames;  // number of frames in file 
h.width  = info.width;      %    int w;       // width (number of vectors for raw, image width for processed data)
h.height = info.height;     %    int h;       // height (number of samples for raw, image height for processed data)
h.ulx = info.ulx;           %    int ulx;     // roi - upper left (x) 
h.uly = info.uly;           %    int uly;  type   // roi - upper left (y) 
h.urx = info.urx;           %    int urx;     // roi - upper right (x) 
h.ury = info.ury;           %    int ury;     // roi - upper right (y) 
h.brx = info.brx;           %    int brx;     // roi - bottom right (x) 
h.bry = info.bry;           %    int bry;     // roi - bottom right (y) 
h.blx = info.blx;           %    int blx;     // roi - bottom left (x) 
h.bly = info.bly;           %    int bly;     // roi - bottom left (y) 
h.dr = info.dr;             %    int dr;      // data rate (fps or prp in Doppler modes)
h.res = info.res;
h.tname = info.tname;
h.xoffset = info.xoffset;
h.yoffset = info.yoffset;
h.zoffset = info.zoffset;

h.iw = h.urx - h.ulx + 1;
h.ih = h.bry - h.ury + 1;

h.res = info.res;
h.tname = info.tname;
h.data = uint8(zeros(h.ih,h.iw,h.frames));

data = dicomread(fullfile(pathname,name));
[ny,nx,nb,nframes] = size(data);

load(fullfile(pathname,'mask.mat'));

for i=1:h.frames
    org = squeeze(data(:,:,:,i));

    if displayflag
        imshow(org);
        ss=sprintf('Frame %d of %d',i,h.frames);
        title(ss);
        drawnow
    end
    
    tmp = uint8(org(:,:,1) .* m0);
%     tmp = uint8(data(:,:,1,i) .* m0);
    A = tmp(h.ury:h.bry,h.ulx:h.urx); %grab the part containing the image
    if fl == 1  
        h.data(:,:,i)=flipud(A);
    elseif fl == 2
        h.data(:,:,i)=fliplr(A);
    else
        h.data(:,:,i)= A;
    end
end
disp('readIMA done.');



