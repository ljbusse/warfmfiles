function [picks,xcenters,ycenters] = picktwo(input, xsize, ysize, plotflag)
% picks two blocks (upper and lower) from the center of an image
% input is an image
% xsize is the number of pixels in x
% ysize is the number of pixwls in y
%

if nargin < 4, plotflag = 0;end

[yorg, xorg] = size(input);
picks = zeros(ysize,xsize,2);

if plotflag
    figure(plotflag); 
    imagesc(input);colormap(gray); colorbar; axis image;
    hold on;
end

    xmin = round((xorg-xsize)/2);
    xmax = xmin+xsize-1;
    yinc = round(yorg/4);
    ycenters = [1:2:4] * yinc;
    xcenters = (xmin+xmax)/2 * ones(2,1);
    for j=1:2
        ymin = ycenters(j)-round(ysize/2);
        ymax = ymin+ysize-1;
        picks(:,:,j) = input(ymin:ymax,xmin:xmax);
        
        if plotflag
            xplt = [xmin xmax xmax xmin xmin];
            yplt = [ymin ymin ymax ymax ymin];
            plot(xplt,yplt,'r');
        end
    end
if plotflag, hold off; end

