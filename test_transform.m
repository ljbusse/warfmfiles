function test_transform( filename)

%% Input Values
%offsetz = 0.0586; offsetx = 0.0138;      % Offset distance of sensor in x and z directions (in m)
%offsetx = .003; offsetz = -0.006;      % Offset distance of sensor in x and z directions (in m)
%offsetx=-0.0447;
%offsetz=-0.0130;
offsetx=-0.042;     %boresight set with probe horizontal
offsetz=0.015;

offsetx=-0.015;     %boresight set with probe vertical
offsetz=0.042;

d1 = 0.04; 
d2 = 0.1;                    % Length and width of the rectangular Image (in meter)

%This is a fake ultrasound image
I = ones(128,300);
II= cumsum(I,2);
II(:,1) = II(:,300);



%% Open data file
fid = fopen(filename);

% Read data from csv file
readline = fgetl(fid);
count = 1;
while readline ~= -1
    [A,COUNT,ERRMSG,NEXTINDEX] = sscanf(readline,'%d,%d,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%f');
    X=A(3);
    Y=A(4);
    Z=A(5);
    yaw=A(6)*pi/180;
    pitch= A(7) *pi/180;
    roll=A(8)*pi/180;
    time = A(9);
    if count >= 200
    if mod(count,25) == 0
        
        [ Xabcd, Yabcd, Zabcd ] = calc_corners( X, Y, Z, yaw, pitch, roll,...
            offsetx, offsetz, d1, d2 );
        
        figure(10);
        scatter3(Xabcd,Yabcd,Zabcd,[10,20,30,40]);hold on;
        scatter3(X,Y,Z,'x');
        drawnow;
        
%        hold off;
        
        xc = [Xabcd(1) Xabcd(2); Xabcd(4) Xabcd(3)];
        yc = [Yabcd(1) Yabcd(2); Yabcd(4) Yabcd(3)];
        zc = [Zabcd(1) Zabcd(2); Zabcd(4) Zabcd(3)];

        S=surface('XData',xc,...
       'YData',yc,...
       'ZData',zc,...
       'CData',II',...
       'FaceColor','texturemap');
    end
    end

    count = count +1;
    readline = fgetl(fid);
end
xlabel('x-coordinate');
ylabel('y-coordinate');
zlabel('z-coordinate');
%axis([-.5,.5,-.5,.5,.5,1.5]);
axis image;
fclose(fid);

