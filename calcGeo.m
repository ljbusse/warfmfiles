function [center,theta] = calcGeo(p1,p2)
% p1 and p2 are [x,y] pairs "complex numbers"
% returns the distance and angle (degrees) between two points
%

%calculate the center point
center = (p1 + p2) /2;

theta = (angle(p2 - p1)* 180/pi) -90.;%convert to degrees


