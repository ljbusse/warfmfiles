function hout = syncFrameTimes_Siemens( t0, frame0, ufilename,displayflag,pathname)
%
% Adding pathname

if nargin < 4, displayflag =1; end
if nargin == 5
    sss=sprintf('%s/init.mat',pathname);
    load(sss);
else
    load('init.mat');
end    

hlimits = h;
clear h;

%open ultrasound file and read all
h=readInfo2(pathname,ufilename);

%open tracker file and match the time to the frames
sss=fullfile(pathname,h.tname);
fid = fopen(sss);

%number of frames to be used
numFrames = h.nframes - frame0;%+1;
timeinc = 1./h.dr;

%allocate some space for output data
hout.X = zeros(numFrames,1);
hout.Y = zeros(numFrames,1);
hout.Z = zeros(numFrames,1);
hout.yaw = zeros(numFrames,1);
hout.pitch = zeros(numFrames,1);
hout.roll = zeros(numFrames,1);
hout.data = uint8(zeros(h.ih,h.iw,numFrames));
hout.res = h.res
hout.xoffset = h.xoffset;
hout.yoffset = h.yoffset;
hout.zoffset = h.zoffset;
hout.tindex = zeros(numFrames,1);

% Read data from csv file
readline = fgetl(fid);
count = 1;
index = 1;
frame = frame0;
timetarget = t0;

fufilename = fullfile(pathname,ufilename);
hh = readIMA3(pathname,ufilename,h.flip,'info.csv',displayflag); %try no left/right flip

f1 = figure(1);
set(f1,'Visible','on');
%draw a background image
% bg = flipud(imread('../pb0.jpg'));
% imagesc([-hlimits.Xmax -hlimits.Xmin],[hlimits.Ymin hlimits.Ymax],bg);
axis([-hlimits.Xmax -hlimits.Xmin hlimits.Ymin hlimits.Ymax]);
axis xy;
grid on;
hold on;

%corner = [-.030 -.003];
corners = [-.030 -.003
    -.030 .003
    .030 .003
    .030 -.003
    -.030 -.003];
e1 = [-.030 0];


while readline ~= -1
    [A,COUNT,ERRMSG,NEXTINDEX] = sscanf(readline,'%d,%d,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%f');
    time = A(9);
    if time < timetarget
        count = count +1;
    else
        if index <= numFrames
            hout.X(index)= A(3);
            hout.Y(index)= A(4); %-
            hout.Z(index)= A(5);
            hout.yaw(index)= A(6);%*pi/180;  -
            hout.pitch(index)= A(7);%*pi/180;
            hout.roll(index)= A(8);%*pi/180; 
            
            
   %        This would be a good place to plot probe position.
            if mod(index,10) == 2
                hold on;
                angle = hout.yaw(index)*pi/180;
                rot = [cos(angle) -sin(angle)
                    sin(angle) cos(angle)];
                ncs = corners * rot;
                e1r = e1 * rot;
                plot(-ncs(:,1)-hout.X(index), ncs(:,2)+hout.Y(index),'b');
                plot(-e1r(1)-hout.X(index),e1r(2) +hout.Y(index),'ro');
        %        rectangle('Position',[-nc(1)-hout.X(index) nc(2)+hout.Y(index) .006 .060]);
                drawnow;

            end
            %        frame
            %        count
            
    
            hout.data(:,:,index) = uint8(hh.data(:,:,frame));
            hout.tindex(index) = count;
            index = index +1;
            frame = frame +1;
        end
        count = count +1;
        timetarget = timetarget + timeinc;
    end
    readline = fgetl(fid);
end
fclose(fid);
%close(f1);
hout.Xavg = mean(hout.X)
hout.Yavg = mean(hout.Y)
%set(f1,'Visible','off');
