function c = calcCorr1a(Y, ref,NX, NY, xsize, ysize)
%
% This function calculates the out-of-plane correlation coefficient 
% for NX * NY regions of interest.
% Y is an input stack if images
% ref is the number of the plane to be used as the reference
% NX and NY are the number of subimages (typically 3x3)
% xsize and ysize are the pixel sizes to be used
%


[ny, nx, nslices] = size(Y);

if ref < 1 || ref > nslices,
    disp('ref is out of range');
    return;
end

c=zeros(nslices,NY,NX);

figure(1);
subplot(3,2,[1 3 5]);
iref = pickimages(Y(:,:,ref),NX,NY, xsize, ysize,1);


%q=pwd;
%qq=fileparts(q)
%bsize = sprintf(': %d x %d',xsize,ysize);
%title([qq(34:end) bsize]);

for i=1:nslices
    hold off;
    img = squeeze( Y(:,:,i));
    figure(2); imagesc(img);colormap(gray); colorbar; axis image;
    hold on;
    text(1,10,sprintf('%3d',i),'Color','White');
    
    ima2 = pickimages(Y(:,:,i),NX,NY, xsize, ysize);
    
    for ix = 1:NX
        for iy = 1:NY
            c(i,iy,ix) = corr2(squeeze(iref(:,:,iy,ix)),squeeze(ima2(:,:,iy,ix)));
        end
    end
    drawnow;
end

figure(1);    
wvals = zeros(NX,1);
for i=1:NY
    subplot(NY,2,i*2);
    x = [1:nslices]*0.05;
    for j=1:NX
        w=findwidth(x,c(:,i,j),0.5);
        dname = sprintf('%4.2f',w);
        plot(x,c(:,i,j),'DisplayName',dname);grid;
        hold on;
    end
   
    legend('show');
end
subplot(NY,2,NX*2);
xlabel('mm');
q=pwd;
qq=fileparts(q)
bsize = sprintf(': %d x %d',xsize,ysize);
suptitle([qq(34:end) bsize]);
hold off;
% Enlarge figure to full screen.
set(gcf, 'Units', 'Normalized', 'OuterPosition', [0 0 1 1]);