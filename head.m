function s = head(filename,numLines)

%% Setup
fid = fopen(filename,'r');
newlineChar = char(10);

try
    %Initialize
    s = '';
    for i=1:numLines
        tmp = fgetl(fid);
        s = [s tmp newlineChar];
    end
%     newChar = fread(fid,1,'*char');
%     numLinesRead = 0;
% 
%     while numLinesRead < numLines
%         s = [newChar s];
% 
%         newChar = fread(fid,1,'*char');
% 
%         if strcmp(newChar,newlineChar)
%             numLinesRead = numLinesRead + 1;
%         end
%         
%         
%     end

catch ME
    %Be sure to close the file regardless of errors
    fclose(fid);
    rethrow(ME);
end

fclose(fid);

end

