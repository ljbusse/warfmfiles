function hout = readSiemensStack2(ufilename)
%read all the frames and coordinates

[f0,t0,tt,ut] = calc_times_siemens(ufilename);
hout = syncFrameTimes_Siemens2( t0, f0, ufilename);