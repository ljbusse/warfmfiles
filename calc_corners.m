function [ Xabcd, Yabcd, Zabcd ] = calc_corners( X, Y, Z, yaw, pitch, roll, offsetx, offsetz, d1, d2 )
%
% calculautes the corners of an ultrasound image given the tracker readings
% and dimensions of image and offsets to from the tracker location to the
% surface of the probe.
% Dimensions are in meters or radians
% 

if nargin < 9,
    d1=0.030; d2 = 0.045;  % Length and width of the rectangular Image (in meter)
end

if nargin < 7,
    offsetx = 0.0586; offsetz = 0.0138; % Offset distance of sensor in x and z directions (in m)
end

%define the corner points
%A = [offsetx;    d1/2;offsetz]; %boresight set with probe horizontal
%B = [offsetx;   -d1/2;offsetz];
%C = [offsetx+d2;-d1/2;offsetz];
%D = [offsetx+d2; d1/2;offsetz];
A = [offsetx;    d1/2;offsetz];  %boresight set with probe vertical
B = [offsetx;   -d1/2;offsetz];
C = [offsetx;-d1/2;offsetz+d2];
D = [offsetx; d1/2;offsetz+d2];

%rotate the points
rot = calc_rotation(yaw, pitch, roll);
AR = rot *A;
BR = rot *B;
CR = rot *C;
DR = rot *D;

%now translate the points
T = [X;Y;Z];
AF = AR + T;
BF = BR + T;
CF = CR + T;
DF = DR + T;

Xabcd = zeros(4,1);
Yabcd = zeros(4,1);
Zabcd = zeros(4,1);

Xabcd(:) = [AF(1), BF(1), CF(1), DF(1)];
Yabcd(:) = [AF(2), BF(2), CF(2), DF(2)];
Zabcd(:) = [AF(3), BF(3), CF(3), DF(3)];



