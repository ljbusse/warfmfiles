function [c, Cmean, Cstd, Cval,XX, YY] = calcCorr1e(Y, sep, NX, NY, xsize, ysize)
% This verion does an in-plane registration before doing the elevation
% calculation.
%
% This function calculates the out-of-plane correlation coefficient 
% for NX * NY regions of interest.
% Y is an input stack of images
% sep is the number of slices separating the planes
% NX and NY are the number of subimages (typically 3x3)
% xsize and ysize are the pixel sizes to be used
%
% 12/15/2017 Adding a test to compare ROI intensities to see if support is
% present (nearly equal intensities).

% Setup the optimizer
[optimizer, metric] = imregconfig('monomodal');
optimizer.MaximumStepLength = 1e-2;
optimizer.MaximumIterations = 200;
xwidth = 100;


[ny, nx, nslices] = size(Y);

if sep < 1 ,
    disp('sep must be greater than 1');
    return;
end


c = zeros(nslices,NY,NX);
support = zeros(NY,NX);

% figure(1);
%subplot(3,2,[1 3 5]);
%iy=3; x=2; is the center of the bottom row

q=2:nslices;
XX      = zeros(length(q),1);
YY      = zeros(length(q),1);
%Cval    = zeros(length(q),1);
Cval = zeros(length(q),NY,NX);

count = 0;
%sum = 0;
for i=q     %sep+1:sep:nslices-2
   
    if i == q(1)
        %look at the central strip of two adjacent images
        F1  = Y(:,:,i-sep);         %previous image
        rF1 = imref2d(size(F1));
        fixed  = pickstrip(F1,xwidth,0);
    end
        
    %look at the central strip of two adjacent images
%    F1  = Y(:,:,i-sep);         %previous image
%    rF1 = imref2d(size(F1));
    
    F2  = Y(:,:,i);             %current image
    rF2 = imref2d(size(F2));

    %central strip of image
    moving = pickstrip(F2,xwidth,0);
    
    %Find transformation to register these images
    Tinc =imregtform(moving, fixed, 'rigid', optimizer, metric);
    
    %Apply transform to 0,0 to calculate the incremental distance between the
    %images
    [XX(i),YY(i)] = transformPointsForward(Tinc,0.0,0.0);
    
    %Apply transform to the current image to allign it with the previous
    [moved,rmoved] = imwarp(F2,rF2,Tinc,'nearest','outputView',rF1);

%     % these plot the difference 
%     figure(10); 
%     imagesc(moved-F1,[-50 50]);colormap(gray); colorbar; axis image;

    %pick grid of images (ROIs)
    iref = pickimages(F1,          NX,NY, xsize, ysize,0);
    ima2 = pickimages(moved,       NX,NY, xsize, ysize,0);
    
    %Calculate the correlation coef for each ROI in the grid
%     ix = 2;
%     iy = 3;
    
    for ix = 1:NX
         for iy = 1:NY
             img1 = squeeze(iref(:,:,iy,ix));
             img2 = squeeze(ima2(:,:,iy,ix));
            c(i,iy,ix) = corr2(img1,img2);
            sumref = sum(sum(img1)); %
            sumima = sum(sum(img2)); %
            % The ratio should be close to one
            rat = sumref/sumima;
                    
            if rat >= 0.85 & rat <= 1.1765
                support(iy,ix) = 1;
            else
                support(iy,ix) = 0;
            end
        end
    end
    count = count +1;
    Cval(count,:,:) = squeeze(c(i,:,:)) .* support; %grab all the regions
%    Cval(count) = c(i,3,2); %bottom row center image
%    Cval(count) = c(i,1,2); %top row center image
%    Cval(count) = c(i,2,2); %center row center image
%   drawnow;

    fixed = moving;
    F1 = F2;
    rF1 = rF2;
end

Cmean = mean(Cval,1);
Cstd = std(Cval,0,1);

disp('calcCorr1e...done');
