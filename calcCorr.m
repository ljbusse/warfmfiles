xboxfunction [ca,cb,cc] = calcCorr(Y, xbox, ybox, xinc, yinc)
%
%

[ny, nx, nslices] = size(Y);
ca=zeros(nslices-1,1);
cb=zeros(nslices-1,1);
cc=zeros(nslices-1,1);

xmin=min(xbox); xmax = max(xbox);

for i=1:nslices-1
    img = squeeze( Y(:,:,i));
    figure(1); imagesc(img);colormap(gray); colorbar; axis image;
    hold on;
    ymin=min(ybox); ymax = max(ybox);
    xplt = [xmin xmax xmax xmin xmin];
    yplt = [ymin ymin ymax ymax ymin];
    plot(xplt,yplt,'b');
    text(1,10,sprintf('%3d',i),'Color','White');

    ima1= squeeze( Y(ybox,xbox,i));
    ima2= squeeze( Y(ybox,xbox,i+1));
    ca(i) = corr2(ima1, ima2);
    ima1= squeeze( Y(ybox+yinc,xbox,i));
    ima2= squeeze( Y(ybox+yinc,xbox,i+1));
    cb(i) = corr2(ima1, ima2);
    yplt = [ymin+yinc ymin+yinc ymax+yinc ymax+yinc ymin+yinc];
    plot(xplt,yplt,'r');
    
    ima1= squeeze( Y(ybox+ 2*yinc,xbox,i));
    ima2= squeeze( Y(ybox+ 2*yinc,xbox,i+1));
    cc(i) = corr2(ima1, ima2);
    yplt = [ymin+2*yinc ymin+2*yinc ymax+2*yinc ymax+2*yinc ymin+2*yinc];
    plot(xplt,yplt,'y');
    hold off;
%    pause;
    drawnow;
end
xvals = 1:nslices-1; 
figure(2); plot(xvals,ca,xvals,cb,xvals,cc);


    