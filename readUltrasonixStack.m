function [ slices,xres,yres ] = readUltrasonixStack( FNAME )
%readUltrasonixStack
%   load all of the png images in the folder FNAME into a 3D array
%   Images are assumed to be in sequential order

curdir = pwd;   %current directory

cd(FNAME);      %change to specified directory

q=dir([FNAME '/*.png']); %get a list of all the png files available

[nfiles, tmp] = size(q);

%Read the first file and get info about sizes etc.
q1=imread(q(1).name);
[ny,nx,nz] = size(q1);

qx1=xml2struct([q(1).name '.xml']);
Xres = qx1.object.regions.region.DeltaPerPixelX;
Yres = qx1.object.regions.region.DeltaPerPixelY;

xres = str2num(Xres.Text);
yres = str2num(Yres.Text);

%these data are specific to the Ultrasonix L14-5/38 probe
if yres == 108
    fov = 50000;
    xstart = 192;
    ystart = 142;
else
    fov = 40000;
    xstart = 150;
    ystart = 150;
end

ny = round(fov/yres);
nx = round(38000/xres);

slices = zeros(ny,nx,nfiles);

slices(:,:,1) = q1(ystart:ystart+ny-1,xstart:xstart+nx-1,1);

for i=2:nfiles
    q1=imread(q(i).name);
    slices(:,:,i) = q1(ystart:ystart+ny-1,xstart:xstart+nx-1,1);
end

cd(curdir); %return to the original directory
