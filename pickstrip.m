function picks = pickstrip(input, xsize, plotflag)
% input is an image
% xsize is the number of pixels in x
%

if nargin < 3, plotflag = 0;end

[yorg, xorg] = size(input);
picks = zeros(yorg,xsize);

if plotflag
    figure(plotflag); 
    imagesc(input);colormap(gray); colorbar; axis image;
    hold on;
end

    xmin = round((xorg-xsize)/2);
    xmax = xmin+xsize-1;
        ymin = 1;
        ymax = yorg;
        picks(:,:) = input(ymin:ymax,xmin:xmax);
        
        if plotflag
            xplt = [xmin xmax xmax xmin xmin];
            yplt = [ymin ymin ymax ymax ymin];
            plot(xplt,yplt,'r');
        end

if plotflag, hold off; end

