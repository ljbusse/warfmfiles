
function c = calcCorr1a(Y, ref, xcenter, ycenter, xsize, ysize)
%
% This function calculates the out-of-plane correlation coefficient 
% for one region of interest.
%


[ny, nx, nslices] = size(Y);

if ref < 1 || ref > nslices,
    disp('ref is out of range');
    return;
end

c=zeros(nslices,1);

xmin=xcenter-xsize/2; xmax = xcenter+xsize/2;
ymin=ycenter-ysize/2; ymax = ycenter+ysize/2;
xplt = [xmin xmax xmax xmin xmin];
yplt = [ymin ymin ymax ymax ymin];

ima1= squeeze( Y(ymin:ymax,xmin:xmax,ref));

for i=1:nslices
    hold off;
    img = squeeze( Y(:,:,i));
    figure(1); imagesc(img);colormap(gray); colorbar; axis image;
    hold on;

    plot(xplt,yplt,'b');
    text(1,10,sprintf('%3d',i),'Color','White');

    ima2= squeeze( Y(ymin:ymax,xmin:xmax,i));
    c(i) = corr2(ima1, ima2);
    %    pause;
    drawnow;
end



    