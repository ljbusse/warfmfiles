function h = readInfo( name,infoFile )
%
%

if nargin < 2, infoFile = 'info.csv'; end

%open the information file to get relavant params
fid = fopen(infoFile);
S= fgetl(fid);      %read the first line

uname = 'DUMMY';
while ~strcmp(upper(name), uname) 
    A = fgetl(fid);
    aa = strsplit(A,',');
    uname = char(aa(2));
end
fclose(fid);

info = dicominfo(uname);

h.nframes = info.NumberOfFrames;
h.width = info.Width;
h.height = info.Height;
h.ulx = str2num(char(aa(4)));%    int ulx;     // roi - upper left (x) 
h.uly = str2num(char(aa(5)));%    int uly;  type   // roi - upper left (y) 
h.urx = str2num(char(aa(6)));%    int urx;     // roi - upper right (x) 
h.ury = str2num(char(aa(5)));%    int ury;     // roi - upper right (y) 
h.brx = str2num(char(aa(4)));%    int brx;     // roi - bottom right (x) 
h.bry = str2num(char(aa(7)));%    int bry;     // roi - bottom right (y) 
h.blx = str2num(char(aa(6)));%    int blx;     // roi - bottom left (x) 
h.bly = str2num(char(aa(7)));%    int bly;     // roi - bottom left (y) 
%h.probe = fread(fp,1,'int');%    int probe;   // probe identifier - additional probe information can be found using this id   
%h.txf = fread(fp,1,'int');%    int txf;     // transmit frequency in Hz
%h.sf = fread(fp,1,'int');%    int sf;      // sampling frequency in Hz
h.dr = str2num(char(aa(8)));%    int dr;      // data rate (fps or prp in Doppler modes)
%h.ld = fread(fp,1,'int');%    int ld;      // line density (can be used to calculate element spacing if pitch and native # elements is known
%h.extra = fread(fp,1,'int');%    int extra;   // extra information (ensemble for color RF)
h.res = str2num(char(aa(10)));
h.tname = char(aa(3));
h.xoffset = str2num(char(aa(11)));
h.yoffset = str2num(char(aa(12)));
h.zoffset = str2num(char(aa(13)));
h.flip    = str2num(char(aa(15)));
% h.forceY = str2num(char(aa(16)));
% h.forceZ = str2num(char(aa(17)));

h.iw = h.urx - h.ulx + 1;
h.ih = h.bry - h.ury + 1;
h.res = str2num(char(aa(10)));
h.tname = char(aa(3));

end

