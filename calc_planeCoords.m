function [ Xrt, Yrt, Zrt ] = calc_planeCoords( Xt, Yt, Zt, yaw, pitch, roll, offsetx, offsetz, d1, d2, nwidth, ndepth)
%
% calculates the XYZ coordinates of an ultrasound image given the tracker readings
% and dimensions of image and offsets to from the tracker location to the
% surface of the probe.
% Dimensions are in meters or radians
% 

if nargin < 9,
    d1=0.030; d2 = 0.045;  % Length and width of the rectangular Image (in meter)
end

if nargin < 7,
    offsetx=-0.015;         %boresight set with probe vertical || to tracker Y
    offsetz=0.040;    
end

% Assuming the boresight set with probe vertical and || to tracker Y
dy = d1/nwidth;
dz = d2/ndepth;

nx = nwidth;
ny = nwidth;
nz = ndepth;

x = -offsetx * ones(1,nx);  %not sure about the minus sign ???
y = dy * [-ny/2:(ny-1)/2];
z = offsetz + dz * [0:nz-1];

[X,Z] = meshgrid(x,z);
Y     = meshgrid(y,z);


rot = calc_rotation(yaw, pitch, roll);
temp= [X(:),Y(:),Z(:)] * rot.';
%temp= [X(:),Y(:),Z(:)];

%now translate the points
T = [Xt,Yt,Zt];
temp = temp + T;

sz=size(X);

Xrt=reshape(temp(:,1),sz);
Yrt=reshape(temp(:,2),sz);
Zrt=reshape(temp(:,3),sz);



% X1 = reshape(Xrt, size(Xrt,1), size(Xrt,2)*size(Xrt,3));
% Y1 = reshape(Yrt, size(Yrt,1), size(Yrt,2)*size(Yrt,3));
% Z1 = reshape(Zrt, size(Zrt,1), size(Zrt,2)*size(Zrt,3));
% plot3(X1, Y1, Z1)
% plot3(Xrt, Yrt, Zrt)
