function [outvol,XX,YY] = alignInPlane( invol)
%this function attempts to perform in-plane registration of images
%currently using box near the top of the volume.


% Setup the optimizer
[optimizer, metric] = imregconfig('monomodal');
optimizer.MaximumStepLength = 1e-2;
optimizer.MaximumIterations = 200;
xwidth = 100;


[ny, nx, nslices] = size(invol);

outvol = uint8(zeros(size(invol)));

% if sep < 1 ,
%     disp('sep must be greater than 1');
%     return;
% end


%c = zeros(nslices,NY,NX);
%support = zeros(NY,NX);

% figure(1);
%subplot(3,2,[1 3 5]);
%iy=3; x=2; is the center of the bottom row

q=2:nslices;
XX      = zeros(length(q),1);
YY      = zeros(length(q),1);
%Cval    = zeros(length(q),1);
%Cval = zeros(length(q),NY,NX);

count = 0;
%sum = 0;
sep = 1;
xpick = [380 670];
ypick = [100 200];
outvol(:,:,1) = invol(:,:,1);

%Total transform
T = affine2d([1 0 0; 0 1 0; 0 0 1]); 

for i=q  
   
    if i == q(1)
        %look at the central strip of two adjacent images
        F1  = invol(:,:,i-sep);         %previous image
        rF1 = imref2d(size(F1));
        fixed  = pickHorStrip(F1,xpick, ypick);
    end
        
    %look at the central strip of two adjacent images
%    F1  = Y(:,:,i-sep);         %previous image
%    rF1 = imref2d(size(F1));
    
    F2  = invol(:,:,i);             %current image
    rF2 = imref2d(size(F2));

    %central strip of image
    moving = pickHorStrip(F2,xpick,ypick);
    
    %Find transformation to register these images
    Tinc =imregtform(moving, fixed, 'rigid', optimizer, metric);
    
    %Apply transform to 0,0 to calculate the incremental distance between the
    %images
    [XX(i),YY(i)] = transformPointsForward(Tinc,0.0,0.0);    
    
    %put together the full transformation
    T = affine2d(Tinc.T * T.T) ;
        
    %Apply full transform to the current image to allign it with the previous
    [moved,rmoved] = imwarp(F2,rF2,T,'nearest','outputView',rF1);


    outvol(:,:,i) = moved;
    
    fixed = moving;
    F1 = F2;
    rF1 = rF2;
end

