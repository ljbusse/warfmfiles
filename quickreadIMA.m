function stack = quickreadIMA(name,fl)
%
%
%

if nargin < 2, fl=0;end


tmp3 = dicomread(name);
[ny,nx,nb,nframes] = size(tmp3);

stack = uint8(zeros(ny,nx,nframes));

for i=1:nframes
    tmp = uint8(tmp3(:,:,1,i));
    if fl == 1  
        stack(:,:,i)=flipud(tmp);
    elseif fl == 2
        stack(:,:,i)=fliplr(tmp);
    else
        stack(:,:,i)= tmp;
    end
end



