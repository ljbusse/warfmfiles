function [outvol,XX,YY,ZZ] = alignVols( V1, V2)
%this function attempts to perform registration of two volumes using info
%near the top.


% Setup the optimizer
[optimizer, metric] = imregconfig('monomodal');
optimizer.MaximumStepLength = 1e-2;
optimizer.MaximumIterations = 200;
xwidth = 100;


[ny1, nx1, nsl1] = size(V1);
[ny2, nx2, nsl2] = size(V2);



xpick = [300 700];%[380 670];
ypick = [300 500];%[100 200];
zpick = [150 450];%[1 min(nsl1,nsl2)];




%F1  = invol(:,:,i-sep);         %previous image
rV1 = imref3d(size(V1));
fixed  = pickVolume(V1,xpick, ypick,zpick);
        
    
%    F2  = invol(:,:,i);             %current image
rV2 = imref3d(size(V2));

moving = pickVolume(V2,xpick,ypick,zpick);
    
%Find transformation to register these images
tic;
Tinc =imregtform(moving, fixed, 'rigid', optimizer, metric);
toc
    
%Apply transform to 0,0 to calculate the incremental distance between the
%images
[XX,YY,ZZ] = transformPointsForward(Tinc,0.0,0.0,0.0);    
    
        
%Apply full transform to the current image to allign it with the previous
[moved,rmoved] = imwarp(V2,rV2,Tinc,'nearest','outputView',rV1);


outvol = uint8(moved);
    


