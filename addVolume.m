function h = addVolume( h,rv1,xl1,yl1,zl1)
x0 = round((xl1(1) - h.Xmin)/h.res);
y0 = round((yl1(1) - h.Ymin)/h.res);
z0 = round((zl1(1) - h.Zmin)/h.res);

%check the limits
x1 = max(1,x0);y1 = max(1,y0);z1 = max(1,z0); %make the beginning index >= 1

xoffset = x0 -x1;
yoffset = y0 -y1;
zoffset = z0 -z1;

[ny,nx,nz]=size(rv1);
[my,mx,mz]=size(h.recVol);

x2 = min(x0+nx-1, x1+nx-1);
y2 = min(y0+ny-1, y1+ny-1);
z2 = min(z0+nz-1, z1+nz-1);

x3 = min(x2,mx);                %make the last index <= size(recVol)
y3 = min(y2,my);
z3 = min(z2,mz);

xo = x2 -x3;                    %calculate offsets
yo = y2 -y3;
zo = z2 -z3;

% this line replaces with the max
h.recVol(y1:y3,x1:x3,z1:z3) = ...
    max(h.recVol(y1:y3,x1:x3,z1:z3), ...
    rv1( 1-yoffset:ny-yo, 1-xoffset:nx-xo, 1-zoffset:nz-zo));

% % These lines replace by the mean where data overlaps
% t1 = (h.recVol(y1:y3,x1:x3,z1:z3));
% t2 = (rv1( 1-yoffset:ny-yo, 1-xoffset:nx-xo, 1-zoffset:nz-zo));
% 
% m1 = find(t1 > 10);
% m2 = find(t2 > 10);
% m3 = intersect(m1,m2);
% mask = zeros(size(t1));
% mask(m1) = 1;
% mask(m2) = 1;
% mask(m3) = 0.5;
% t3 = double(t1 + t2) .* mask;
% 
% h.recVol(y1:y3,x1:x3,z1:z3) = uint8(t3);

end

