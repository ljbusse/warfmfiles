function rot = calc_rotation( yaw, pitch, roll)
%
% calculautes the rotation matrix IS doc page 68
% 

CP = cos(pitch);
CY = cos(yaw);
CR = cos(roll);
SP = sin(pitch);
SY = sin(yaw);
SR = sin(roll);

rot = zeros(3,3) ;

rot = [CP*CY 	SR*SP*CY-CR*SY 	CR*SP*CY+SR*SY;...
	   CP*SY    SR*SP*SY+CR*CY  CR*SP*SY-SR*CY;...
	   -SP      CP*SR           CP*CR];


end

