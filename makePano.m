function makePano(b8path, b8name,flipflag)
% b8path    = path to location of the data file
% b8name    = name of the ultrasonix .b8 file
% flipflag  = 0 do nothing
%           = 1 flipud
%
% Written by L J Busse, ljb@ljbdev.com
%

%cd '/home/ljb/Projects/WARF/WARFdata/phantom5/10-30-2017-Generic'
%h=readb8('pano-one.b8',0); %OK
%h=readb8('pano-two.b8',0); %OK
%h=readb8('pano-multi.b8',0); %doesn't work
cd b8path;
h=readb8(b8name,flipflag); %OK

[optimizer, metric] = imregconfig('monomodal');
optimizer.MaximumStepLength = 1e-2;
optimizer.MaximumIterations = 200;


[ysizeorg,xsizeorg,nslices]=size(h.data);

ycenter = fix(ysizeorg/2);
xcenter = fix(xsizeorg/2);

%width of the strips used for image registration
xwidth = 100;
%ywidth = 200;

xstart = xcenter;
ystart = ycenter;

xo2 = fix(xsizeorg/2);
yo2 = fix(ysizeorg/2);

padx = round(5*ysizeorg);
F1 = squeeze(h.data(:,:,1));
I = padarray(F1',[200 padx],'post');
I(:,ycenter+xwidth:end) = 0;
RI = imref2d(size(I));

%Total transform
T = affine2d([1 0 0; 0 1 0; 0 0 1]); 

% figure(16);imshow(moved,rmoved,[]); colormap(gray); colorbar; axis image;
kfirst = 2;
klast = nslices-4;
for k=kfirst:1:klast
    if k == kfirst
        F1 = squeeze(h.data(:,:,k-1));
        fullf = F1';
        fixed = pickstrip(F1',xwidth,0);
        title(num2str(k));
    end
    
    F2 = squeeze(h.data(:,:,k));
    moving = pickstrip(F2',xwidth,0);
    title(num2str(k));
    
    %Incremental transforms between 2 adjacent images
    Tinc =imregtform(moving, fixed, 'rigid', optimizer, metric);
    
    %put together the full transformation
    T = affine2d(Tinc.T * T.T) ; 
    
    if k==klast
        %apply the transform to the right half of the last image
        tmp = F2';
        rhi = tmp(:,yo2:end);
%        rhi=tmp
        rrhi = imref2d(size(rhi));
        [moved,rmoved] = imwarp(rhi,rrhi,T,'nearest','outputView',RI);

    else
        [moved,rmoved] = imwarp(moving,imref2d(size(moving)),T,'nearest','outputView',RI);
    end
    moved = imtranslate(moved, rmoved, [yo2 0]);   
%    figure(16);imshow(moved,rmoved,[]); colormap(gray); colorbar; axis image;

    C = combine(I,moved,'average');
   
    figure(17);imshow(C,[]); colormap(gray); axis image; drawnow;
    
    I=C;                    %update main image
    fixed = moving;         %update fixed
    
end
colorbar;