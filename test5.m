%[data,xres,yres]=readUltrasonixStack('.');
%h=readb8('l1.b8');
%h=readb8('short-pan.b8');

%Following 3 files in ljb2
%h=readb8('belly.b8');
%h=readb8('leg-long.b8');
h=readb8('leg-curve.b8');


[optimizer, metric] = imregconfig('monomodal');
optimizer.MaximumStepLength = 1e-2;
optimizer.MaximumIterations = 100;

[ysizeorg,xsizeorg,nslices]=size(h.data);

ycenter = fix(ysizeorg/2);
xcenter = fix(xsizeorg/2);

%width of the strips used for image registration
xwidth = 30;
ywidth = 200

%put F1 in Final

Final = zeros(1000, 3000);
%ftemp = zeros(1000, 3000);
%testI = zeros(99,99);
%testI(50,50) = 1;

F1 = squeeze(h.data(:,:,1));    
fixed  = pickstrip(F1', xwidth, 0);

I = padarray(F1',[300,300],'both');
RI = imref2d(size(I));

% is the first image...put it all in Final
deltax = 0;
deltay = 0;
degree = 0;
xstart = 500-xcenter;
ystart = 500-ycenter;

xo2 = round(xsizeorg/2);
yo2 = round(ysizeorg/2);
for k=2:nslices-2
%    ftemp = Final;
    F1 = squeeze(h.data(:,:,k-1));
    [fixed,xf,yf]    = picktwo(F1', xwidth, ywidth, 1);
    title(num2str(k));
    F2 = squeeze(h.data(:,:,k));
    [moving,xm,ym]   = picktwo(F2', xwidth, ywidth, 2);

    top = imregtform(moving(:,:,1), fixed(:,:,1),'rigid', optimizer, metric);
    bot = imregtform(moving(:,:,2), fixed(:,:,2),'rigid', optimizer, metric);
    
    %try applying the transform to learn translation and rotation
    [t1,t2] = transformPointsForward(top,xm(1),ym(1));
    [b1,b2] = transformPointsForward(bot,xm(2),ym(2));

    tp = complex(t1,t2);
    bp = complex(b1,b2);
    
    [trans, deg]=calcGeo(tp,bp);
    degree = degree - deg
    
    moved = imrotate(F2',degree,'bicubic','crop');
    xx = real(trans) - xm(1);
    yy = imag(trans) - mean(ym);
    moved = imtranslate(moved,[xx,yy],'OutputView','same');
    
    deltay = deltay + round(yy);
    deltax = deltax + round(xx);
    
    %Adjust the right half of Final
    x1 = xstart + deltay;
    y1 = yo2 + ystart + deltax;

    %the right hallf of moved
    %qq=moved(:,yo2:end);
    
    %Final(x1:x1+xsizeorg-1, y1:y1+yo2-1) = qq;
    
    %figure(14);imagesc(Final); colormap(gray); colorbar; axis image; drawnow;
    figure(14);imshow(moved,RI,[]); colormap(gray); colorbar; axis image; drawnow;

    
    drawnow;

    
end

