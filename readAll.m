function h = readInfo( infoFile )
%
%

if nargin < 1, infoFile = 'info.csv'; end

%open the information file to get relavant params
fid = fopen(infoFile);
S= fgetl(fid);      %read the first line
aa = strsplit(S,',');
nfiles = str2num(char(aa(1)));

for i = 1:nfiles   
    S = fgetl(fid);
    aa = strsplit(S,',');
    uname = char(aa(2));
    count = str2num(char(aa(1)));
    
    if count == 1,
        info = dicominfo(uname);        
        h.nframes = nfiles;
        h.width = info.Width;
        h.height = info.Height;
        h.ulx = str2num(char(aa(4)));%    int ulx;     // roi - upper left (x)
        h.uly = str2num(char(aa(5)));%    int uly;  type   // roi - upper left (y)
        h.urx = str2num(char(aa(6)));%    int urx;     // roi - upper right (x)
        h.ury = str2num(char(aa(5)));%    int ury;     // roi - upper right (y)
        h.brx = str2num(char(aa(4)));%    int brx;     // roi - bottom right (x)
        h.bry = str2num(char(aa(7)));%    int bry;     // roi - bottom right (y)
        h.blx = str2num(char(aa(6)));%    int blx;     // roi - bottom left (x)
        h.bly = str2num(char(aa(7)));%    int bly;     // roi - bottom left (y)
        h.dr = str2num(char(aa(8)));%    int dr;      // data rate (fps or prp in Doppler modes)
        h.res = str2num(char(aa(10)));
        h.tname = char(aa(3));
        h.xoffset = str2num(char(aa(11)));
        h.yoffset = str2num(char(aa(12)));
        h.zoffset = str2num(char(aa(13)));
        h.iw = h.urx - h.ulx + 1;
        h.ih = h.bry - h.ury + 1;
        h.res = str2num(char(aa(10)));
        h.tname = char(aa(3));
        data = dicomread(uname);
        tmp = data(:,:,1);
        A = tmp(h.ury:h.bry,h.ulx:h.urx); %grab the part containing the image
        [ny,nx] = size(A);
        h.data = zeros(ny,nx,nfiles);
    end
    data = dicomread(uname);
    tmp = data(:,:,1);
    A = tmp(h.ury:h.bry,h.ulx:h.urx); %grab the part containing the image
    h.data(:,:,i) = A;
    
end
fclose(fid);

end

