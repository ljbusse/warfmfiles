function [ Xabcd, Yabcd, Zabcd ] = calc_corners( X, Y, Z, yaw, pitch, roll, offsetx, offsetz, d1, d2 )
%
% calculautes the corners of an ultrasound image given the tracker readings
% and dimensions of image and offsets to from the tracker location to the
% surface of the probe.
% Dimensions are in meters or radians
% 

if nargin < 9,
    d1=0.030; d2 = 0.045;  % Length and width of the rectangular Image (in meter)
end

if nargin < 7,
    offsetx = 0.0586; offsetz = 0.0138; % Offset distance of sensor in x and z directions (in m)
end

CP = cos(pitch);
CY = cos(yaw);
CR = cos(roll);
SP = sin(pitch);
SY = sin(yaw);
SR = sin(roll);

xyz=zeros(4,3);                        % Solution matrix for calculating the x,y and z-coordinates of points A, B, C and D respectively 

% Coordinates of point A
xyz(1,1) = X + offsetx*CP*CY + (d1/2)*(CY*SP*SR - SY*CR) + offsetz*(SY*SR + CY*SP*CR);
xyz(1,2) = Y + offsetx*SY*CP + (d1/2)*(CY*CR + SY*SP*SR) + offsetz*(SY*SP*CR - CY*SR);
xyz(1,3) = Z - offsetx*SP    + (d1/2)*CP*SR              + offsetz*CP*CR                                ;

% Coordinates of point B
xyz(2,1) = X + offsetx*CP*CY + (-d1/2)*(CY*SP*SR - SY*CR) + offsetz*(SY*SR + CY*SP*CR);
xyz(2,2) = Y + offsetx*SY*CP + (-d1/2)*(CY*CR + SY*SP*SR) + offsetz*(SY*SP*CR - CY*SR);
xyz(2,3) = Z - offsetx*SP    + (-d1/2)*CP*SR              + offsetz*CP*CR                                ;

% Coordinates of point C
xyz(3,1) = X + (offsetx+d2)*CP*CY + (-d1/2)*(CY*SP*SR - SY*CR) + offsetz*(SY*SR + CY*SP*CR);
xyz(3,2) = Y + (offsetx+d2)*SY*CP + (-d1/2)*(CY*CR + SY*SP*SR) + offsetz*(SY*SP*CR - CY*SR);
xyz(3,3) = Z - (offsetx+d2)*SP    + (-d1/2)*CP*SR              + offsetz*CP*CR                                ;

% Coordinates of point D
xyz(4,1) = X + (offsetx+d2)*CP*CY + (d1/2)*(CY*SP*SR - SY*CR) + offsetz*(SY*SR + CY*SP*CR);
xyz(4,2) = Y + (offsetx+d2)*SY*CP + (d1/2)*(CY*CR + SY*SP*SR) + offsetz*(SY*SP*CR - CY*SR);
xyz(4,3) = Z - (offsetx+d2)*SP    + (d1/2)*CP*SR              + offsetz*CP*CR                                ;

Xabcd = xyz(:,1);
Yabcd = xyz(:,2);
Zabcd = xyz(:,3);


end

