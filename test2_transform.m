function [minvox, maxvox] = test2_transform( tfilename, ufilename)

%% Input Values
%offsetx = 0.0586; offsetz = 0.0138;      % Offset distance of sensor in x and z directions (in m)
offsetx=-0.040;     %boresight set with probe horizontal
offsetz=0.015;

offsetx=-0.015;     %boresight set with probe vertical
offsetz=0.040;


d1=0.038; d2 = 0.040;                    % Length and width of the rectangular Image (in meter)

[f0,t0,tt,ut] = calc_times(tfilename,ufilename);

hout = syncFrameTimes( t0, f0, tfilename, ufilename);

[nx,ny,nf] =size(hout.data);
finc = 1           %display every finc frame
maxx = -5000; minx = 5000;
maxy = -5000; miny = 5000;
maxz = -5000; minz = 5000;
for i=1:finc:nf
    X=hout.X(i);
    Y=hout.Y(i);
    Z=hout.Z(i);
    yaw=hout.yaw(i);
    pitch=hout.pitch(i);
    roll=hout.roll(i);
    II = fliplr(squeeze(hout.data(:,:,i)));
    
    [ Xabcd, Yabcd, Zabcd ] = calc_corners( X, Y, Z, yaw, pitch, roll,...
        offsetx, offsetz, d1, d2 );
    maxx=max(max(Xabcd),maxx); minx=min(min(Xabcd),minx);
    maxy=max(max(Yabcd),maxy); miny=min(min(Yabcd),miny);
    maxz=max(max(Zabcd),maxz); minz=min(min(Zabcd),minz);
    
    figure(10);
    scatter3(Xabcd,Yabcd,Zabcd,[10,20,30,40]);hold on;
    drawnow;
    
    xc = [Xabcd(1) Xabcd(2); Xabcd(4) Xabcd(3)];
    yc = [Yabcd(1) Yabcd(2); Yabcd(4) Yabcd(3)];
    zc = [Zabcd(1) Zabcd(2); Zabcd(4) Zabcd(3)];

    S=surface('XData',xc,...
   'YData',yc,...
   'ZData',zc,...
   'CData',II,...
   'FaceColor','texturemap');

end
xlabel('x-coordinate');
ylabel('y-coordinate');
zlabel('z-coordinate');
%xmin = min(hout.X);
%xmax = max(hout.X);
%ymin = min(hout.Y);
%ymax = max(hout.Y);
%zmin = min(hout.Z);
%zmax = max(hout.Z);

%axis([xmin xmax ymin ymax zmin zmax]);

minvox = [minx,miny,minz];
maxvox = [maxx,maxy,maxz];   %bounding volume

axis image;
colormap gray;
