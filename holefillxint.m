function out = holefillxint(in)
%
%

[nx,ny,nz]=size(in);
out = in;

for iy = 1:ny
    for iz = 1:nz
        tmp = in(:,iy,iz);
        xnz = find(tmp~=0);
        if length(xnz) > 50;
            xz  = find(tmp==0);
            qq = interp1(xnz,tmp(xnz),xz); % some values will be NaN
            xgood = find(qq < 256);
            out(xz(xgood),iy,iz) = qq(xgood);
        end
    end
end
        


                

end

