function hout = syncFrameTimes_Siemens2( t0, frame0, ufilename)
%
%
%open ultrasound file and read all
h=readInfo(ufilename);

%open tracker file and match the time to the frames
fid = fopen(h.tname);

%number of frames to be used
numFrames = h.nframes - frame0;%+1;
timeinc = 1./h.dr;

[xfit,yfit,zfit] = plotTracker(h.tname,h.dr, timeinc*numFrames,0);

%allocate some space for output data
hout.X = zeros(numFrames,1);
hout.Y = zeros(numFrames,1);
hout.Z = zeros(numFrames,1);
hout.yaw = zeros(numFrames,1);
hout.pitch = zeros(numFrames,1);
hout.roll = zeros(numFrames,1);
hout.data = uint8(zeros(h.ih,h.iw,numFrames));
hout.res = h.res
hout.xoffset = h.xoffset;
hout.yoffset = h.yoffset;
hout.zoffset = h.zoffset;
hout.tindex = zeros(numFrames,1);


fx = true; 
fy = true; 
fz = true; 
if h.forceX == 0; fx = false;end
if h.forceY == 0; fy = false;end
if h.forceZ == 0; fz = false;end

% Read data from csv file
readline = fgetl(fid);
count = 1;
index = 1;
frame = frame0;
timetarget = t0;

hh = readIMA(ufilename); %try no left/right flip

while readline ~= -1
    [A,COUNT,ERRMSG,NEXTINDEX] = sscanf(readline,'%d,%d,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%f');
    time = A(9);
    if time < timetarget
        count = count +1;
    else
        if index <= numFrames
            if (fx); hout.X(index) = h.forceX; else; hout.X(index) = xfit(index)/1000; end
            if (fy); hout.Y(index) = h.forceY; else; hout.Y(index) = yfit(index)/1000; end
            if (fz); hout.Z(index) = h.forceZ; else; hout.Z(index) = zfit(index)/1000; end
%             hout.X(index)= .100;%A(3); %Just a constant for testing purposes
%             hout.Y(index)= yfit(index)/1000;%A(4); %-
%             hout.Z(index)= zfit(index)/1000;%A(5);
            hout.yaw(index)= A(6);%*pi/180;  -
            hout.pitch(index)= A(7);%*pi/180;
            hout.roll(index)= A(8);%*pi/180; -
    %        index
    %        frame
    %        count
            hout.data(:,:,index) = hh.data(:,:,frame);
            hout.tindex(index) = count;
            index = index +1;
            frame = frame +1;
        end
        count = count +1;
        timetarget = timetarget + timeinc;
    end
    readline = fgetl(fid);
end
fclose(fid);
