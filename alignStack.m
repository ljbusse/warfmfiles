function [outstack, XX, YY] = alignStack(Y)


% Setup the optimizer
[optimizer, metric] = imregconfig('monomodal');
optimizer.MaximumStepLength = 1e-2;
optimizer.MaximumIterations = 200;
xwidth = 400;


[ny, nx, nslices] = size(Y);
outstack = uint8(zeros( size(Y)));

sep = 1;
%support = zeros(NY,NX);

% figure(1);
%subplot(3,2,[1 3 5]);
%iy=3; x=2; is the center of the bottom row

q=2:nslices;
XX      = zeros(length(q),1);
YY      = zeros(length(q),1);
%Cval    = zeros(length(q),1);
%Cval = zeros(length(q),NY,NX);

count = 0;
%sum = 0;
for i=q     %sep+1:sep:nslices-2
   
    if i == q(1)
        %look at the central strip of two adjacent images
        F1  = Y(:,:,i-sep);         %previous image
        rF1 = imref2d(size(F1));
%        fixed  = pickstrip(F1,xwidth,1);
        fixed = squeeze(pickimages(F1,1,1,100,300,1)); %One ROI
    end
        
    %look at the central strip of two adjacent images
%    F1  = Y(:,:,i-sep);         %previous image
%    rF1 = imref2d(size(F1));
    
    F2  = Y(:,:,i);             %current image
    rF2 = imref2d(size(F2));

    %central strip of image
%    moving = pickstrip(F2,xwidth,2);
%    moving = squeeze(pickimages(F2,1,1,300,300,0));    drawnow;
    moving = squeeze(pickimages(F2,1,1,100,300,0));    drawnow;
    
    support = max(max(F2));
    
    if support >= 100
        %Find transformation to register these images
        Tinc =imregtform(moving, fixed, 'rigid', optimizer, metric);

        %Apply transform to 0,0 to calculate the incremental distance between the
        %images
        [XX(i),YY(i)] = transformPointsForward(Tinc,0.0,0.0);

        %Apply transform to the current image to allign it with the previous
        [moved,rmoved] = imwarp(F2,rF2,Tinc,'nearest','outputView',rF1);

        outstack(:,:,i) = moved;
        figure(3);     imagesc(moved);colormap(gray); colorbar; axis image;
    else
        outstack(:,:,i) = F2;
    end

    fixed = moving;
    F1 = F2;
    rF1 = rF2;
end


