function [recVol,xl,yl,zl] = recVolume_siemens( ufilename,dx,align)
%this program is used to reconstruct 3D volume images based on tracker data only.
%Input arguments are the tracker filename, ultrasound filename and the
%final resolution desired.

%set the resolution
if nargin  < 2, dx = 0.0005;end
if nargin < 3, align = 0; end

dy = dx;	%make resolution uniform in all directions
dz = dx;	

%read all the frames and coordinates
[f0,t0,tt,ut] = calc_times_siemens(ufilename);
hout = syncFrameTimes_Siemens( t0, f0, ufilename);

if align == 1
    [stack,XX,YY]=alignStack(hout.data);
    hout.data = stack;
    Y0=hout.Y(1);
    hout.Y = Y0 + cumsum(XX) * dx/1000;
end

offsetx=hout.xoffset/1000;%-0.015;     	%boresight set with probe vertical and || to Y tracker axis
offsetz=hout.zoffset/1000;%0.040;


%adjust to accomodate the US image
[ny,nx,nf] = size(hout.data);

res = hout.res/1000;
d1 = res*nx;            	%width of image (meters)
d2 = res*ny;                %adjust the depth to the FOV used.

%determine the overall extent of the volume to be reconstructed (in tracker coordinates)
xmin = min(hout.X) - offsetx;
ymin = min(hout.Y) - d1/2;
zmin = min(hout.Z) + offsetz;
xmax = max(hout.X) - offsetx;
ymax = max(hout.Y) + d1/2;
zmax = max(hout.Z) + offsetz;

zmax = zmax + (res * ny);   %Add a bit to accomodate the US image
ymin = ymin - (res * nx/2);
ymax = ymax + (res * nx/2);

NX = ceil((xmax - xmin)/dx);
NY = ceil((ymax - ymin)/dy);
NZ = ceil((zmax - zmin)/dz);

%setup a 3D volume for reconstructed output
recVol = zeros(NX,NY,NZ);       %array to accumulate image data.
recHit = 10000*ones(NX,NY,NZ);	%array to keep track of the number of times data is placed in this element

% downsample the raw image data
sf = res/dx
[ny,nx,nf] = size(hout.data)
for i=1:nf
	scaledImg = imresize(hout.data(:,:,i),sf);
	if i==1
		[sy,sx] = size(scaledImg);
		rawVol = zeros(sy,sx,nf);
	end
	rawVol(:,:,i) = max(0,scaledImg);
end

%convert angles to radians
tmp = hout.yaw;   hout.yaw = tmp * pi/180;
tmp = hout.pitch; hout.pitch = tmp * pi/180;
tmp = hout.roll;  hout.roll = tmp * pi/180;


%Now ready to place the images into recVol
for i=1:nf
	[ Xrt, Yrt, Zrt ] = calc_planeCoords( hout.X(i), hout.Y(i), hout.Z(i), ...
   		hout.yaw(i), hout.pitch(i), hout.roll(i), offsetx, offsetz, d1, d2,sx,sy);
%       		0, 0, 0, offsetx, offsetz, d1, d2,sx,sy);

	%convert the planar coordinates to indices
	Xind = round((Xrt - xmin)/dx);
	Yind = round((Yrt - ymin)/dy);
	Zind = round((Zrt - zmin)/dz);

	%test the range of the indices
    maxXind = max(max(Xind));
    maxYind = max(max(Yind));
    maxZind = max(max(Zind));
    minXind = min(min(Xind));
    minYind = min(min(Yind));
    minZind = min(min(Zind));
    
    for iy = 1:sy
		for ix = 1: sx
            xx = Xind(iy,ix);
            yy = Yind(iy,ix);
            zz = Zind(iy,ix);
            
            if (xx > 0) && (xx <= NX)
                if (yy > 0) && (yy <= NY)
                    if (zz > 0) && (zz <= NZ)
                        recVol(xx,yy,zz) = recVol(xx,yy,zz) + rawVol(iy,ix,i);
                        q=recHit(xx,yy,zz);
                        if q == 10000; q=0;end; %first time this voxel used
                        recHit(xx,yy,zz) = q+1;
                    end
                end
            end
        end
    end
end


%normalize
A = recVol ./ recHit;

%do some hole filling
recVol = holefill(A);
xl = [xmin,xmax];
yl = [ymin,ymax];
zl = [zmin,zmax];



