function Yout = speckleReduce(Y, iter, step)
%
%

[ny, nx, nslices] = size(Y);
Yout = uint8(zeros(size(Y)));

for i=1:nslices
    tmp = double(squeeze(Y(:,:,i)));
    tmp = gf3( tmp , iter, step);
    Yout(:,:,i) = uint8(tmp);
end