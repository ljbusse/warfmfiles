function [xfit,yfit,zfit]=plotTracker(filename, fr, duration,pflag);
%
%defaults for Siemens UTdata
if nargin < 4, pflag = 1; end
if nargin < 3, duration = 30; end
if nargin < 2, fr = 20.41; end

A=csvread(filename);
X=1000*A(:,3);
Y=1000*A(:,4);
Z=1000*A(:,5);
Yaw=A(:,6);
Pitch=A(:,7);
Roll=A(:,8);
[N,M] = size(A);

time = A(:,9);
npts = length(time);

tstop = time(npts);
list = find (time >= tstop - duration);
p1 = list(1);
p2 = npts;
tstart = time(list(1));

dt = 1/fr;
tt = time(p1):dt:time(p2);


P = polyfit(time(p1:p2),X(p1:p2),2);
xfit = polyval(P,tt);

P = polyfit(time(p1:p2),Y(p1:p2),2);
yfit = polyval(P,tt);

P = polyfit(time(p1:p2),Z(p1:p2),2);
zfit = polyval(P,tt);

if pflag == 1
    h=figure(1);
    set(h,'Position',[312 54 1329 920]);

    subplot(2,2,1);
    plot(time(p1:p2),X(p1:p2),time(p1:p2),Y(p1:p2), tt,xfit,'b-',tt,yfit,'r-');
    legend('X','Y');grid;
    title('X & Y');

    subplot(2,2,2)
    plot(time(p1:p2),Z(p1:p2),tt,zfit,'b-');grid;
    title('Z');

    subplot(2,2,3)
    plot(time(p1:p2),Yaw(p1:p2),time(p1:p2),Pitch(p1:p2),time(p1:p2),Roll(p1:p2));
    legend('Yaw','Pitch','Roll');grid;
    title('Angles')

    path = pwd;
    qqq = [path '/' filename];
    suptitle(qqq);
    name = [filename '.png'];
    print(1,'-dpng',name);
end

