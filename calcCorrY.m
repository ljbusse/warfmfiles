function c = calcCorrY(Y, ref, xcenter, ycenter, xsize, ysize)
%
% This function calculates the in-plane correlation coefficient 
% for one region of interest in the Y (i.e. vertical) direction
%


[ny, nx, nslices] = size(Y);

if ref < 1 || ref > nslices,
    disp('ref is out of range');
    return;
end

c=zeros(200,1);

xmin=xcenter-xsize/2; xmax = xcenter+xsize/2;
ymin=ycenter-ysize/2; ymax = ycenter+ysize/2;
xplt = [xmin xmax xmax xmin xmin];
yplt = [ymin ymin ymax ymax ymin];

ima1= squeeze( Y(ymin:ymax,xmin:xmax,ref));


    hold off;
    img = squeeze( Y(:,:,ref));
    figure(1); imagesc(img);colormap(gray); colorbar; axis image;
    hold on;

    plot(xplt,yplt,'b');
    text(1,10,sprintf('%3d',ref),'Color','White');

    for i = 1:200
        ymin = ycenter - 100 + i;   if ymin <  0, ymin =  0; end
        ymax = ymin + ysize;        if ymax > ny, ymax = ny; end
        xplt = [xmin xmax xmax xmin xmin];
        yplt = [ymin ymin ymax ymax ymin];

        ima2= squeeze( Y(ymin:ymax,xmin:xmax,ref));
        c(i) = corr2(ima1, ima2);
        plot(xplt,yplt,'r');
        %    pause;
        drawnow;
    end


    