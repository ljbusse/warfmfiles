function out = holefill(in)
%
%

[nx,ny,nz]=size(in);
out = in;
for ix=1+5:nx-5
    for iy = 1+5:ny-5
        for iz = 1+5:nz-5
            if in(ix,iy,iz) == 0
                tmp = in(ix-2:ix+2, iy-2:iy+2, iz-2:iz+2);
                l = find(tmp~=0);
                ll = length(l);
                if ll > 0
                    tsum = sum(sum(sum(tmp)));
                    out(ix,iy,iz) = tsum/ll;     %replace with the average 
                end
            end
        end
    end
end

                

end

