function [picks, xcenters, ycenters] = pickimages(input, nx, ny, xsize, ysize, plotflag)
% input is an image
% nx is the number of sub-images in the x direction
% ny is the number of sub-images in the y direction
% xsize is the number of pixels in x
% ysize is the number of pixels in y
% picks are the individuals images
% xcenters ycenters are the pixel coordinates of the images.

if nargin < 6, plotflag = 0;end

[yorg, xorg] = size(input);
picks = uint8(zeros(ysize,xsize,ny, nx));

xinc = round(xorg/(2*nx));
xcenters = [1:2:2*nx] * xinc;

yinc = round(yorg/(2*ny));
ycenters = [1:2:2*ny] * yinc;

if plotflag
    figure(plotflag); 
    imagesc(input);colormap(gray); colorbar; axis image;
    hold on;
end

for i =1:nx
    xmin = xcenters(i)-round(xsize/2);
    xmax = xmin+xsize-1;
    for j=1:ny
        ymin = ycenters(j)-round(ysize/2);
        ymax = ymin+ysize-1;
        picks(:,:,j,i) = input(ymin:ymax,xmin:xmax);
        
        if plotflag
            xplt = [xmin xmax xmax xmin xmin];
            yplt = [ymin ymin ymax ymax ymin];
            plot(xplt,yplt,'r');
        end
    end
end
if plotflag, hold off; end

