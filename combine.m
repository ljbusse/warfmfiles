function C = combine( A, B, method )

if nargin < 3
    method = 'average'
end

A = max(A,0);
B = max(B,0);

if method == 'average'
    w = ones(size(A));
    w(B > 0 & A ~= 0) = 0.5;
%     f = logical(A);
%     m = logical(B);
%     op = and(f,m);
%     ad = xor(m,op);
%     cp = xor(f,op);
%     op = and(f,m);
%    C = cp.*A + ad.*B + op.*(A + B)/2;
    C = (A + B) .*w;
end

if method == 'replace'
    f = logical(A);
    m = logical(B);
    op = and(f,m);
    cp = xor(f,op);
    C = cp.*A + m.*B;
end

if method == 'maximum'
    C = max(A,B);
end

end



