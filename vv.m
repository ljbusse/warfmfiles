function vv(in)
[ny,nx,nz] = size(in);
tmp = uint8(zeros([ny,nx,nz]));

Z=nz:-1:1;
for i=1:nz
    t1 = squeeze(in(:,:,i));
    tmp(:,:,Z(i)) = t1;
end

volumeViewer(tmp);
