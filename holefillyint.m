function out = holefillyint(in)
%
%

[nx,ny,nz]=size(in);
out = in;

for ix = 1:nx
    for iz = 1:nz
        tmp = double(in(ix,:,iz));
        ynz = find(tmp~=0);
        if length(ynz) > 50;
            yz  = find(tmp==0);
            qq = interp1(ynz,tmp(ynz),yz); % some values will be NaN
            ygood = find(qq < 256);
            out(ix,yz(ygood),iz) = uint8(qq(ygood));
        end
    end
end
        


                

end

