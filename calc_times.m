function [frame0, t0, tracker_times, ultrasound_times] = calc_times( tfilename, ufilename)
%
%

%open tracker file and read the top and bottom
[status,top]   = system(['head -n 1 ' tfilename]);
[status,bottom]= system(['tail -n 1 ' tfilename]);
[A,COUNT,ERRMSG,NEXTINDEX] = sscanf(top,'%d,%d,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%f');
t_first = A(9);
[A,COUNT,ERRMSG,NEXTINDEX] = sscanf(bottom,'%d,%d,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%f');
t_last = A(9);

%open ultrasound file and read header
h=readb8header(ufilename);
u_first = 0;
u_last  = h.frames/h.dr;    %number of frames / frames per second

plotTracker(tfilename, h.dr, u_last);

tracker_times = [t_first t_last];
ultrasound_times = [u_first u_last];

TT = t_last - t_first;  
UT = u_last - u_first;

if TT < UT
    t0 = t_first;
    frame0 = round(h.frames - h.dr*TT);
end
if TT >= UT
    frame0 =1;
    t0 = t_last - (h.frames/h.dr);
end