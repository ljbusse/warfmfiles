function out = pickVolume(in, xpick, ypick,zpick);
%
%

out = in(ypick(1):ypick(2),xpick(1):xpick(2),zpick(1):zpick(2));
