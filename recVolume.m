function [recVol,xmin,ymin,zmin] = recVolume( tfilename, ufilename,dx)
%this program is used to reconstruct 3D volume images based on tracker data only.
%Input arguments are the tracker filename, ultrasound filename and the
%final resolution desired.

%set the resolution
if nargin  < 3, dx = 0.0005;end

dy = dx;	%make resolution uniform in all directions
dz = dx;	

d1=0.038;             	%width of L14-8 array (in meter)
offsetx=-0.015;     	%boresight set with probe vertical and || to Y tracker axis
offsetz=0.040;

%read all the frames and coordinates
[f0,t0,tt,ut] = calc_times(tfilename,ufilename);
hout = syncFrameTimes( t0, f0, tfilename, ufilename);

%determine the overall extent of the volume to be reconstructed (in tracker coordinates)
xmin = min(hout.X) - offsetx;
ymin = min(hout.Y) - d1/2;
zmin = min(hout.Z) + offsetz;
xmax = max(hout.X) - offsetx;
ymax = max(hout.Y) + d1/2;
zmax = max(hout.Z) + offsetz;

%adjust to accomodate the US image
[ny,nx,nf] = size(hout.data);
d2 = ny*d1/nx;          %adjust the depth to the FOV used.
res = hout.res/1000
zmax = zmax + (res * ny); %Add a bit to accomodate the US image
ymin = ymin - (res * nx/2);
ymax = ymax + (res * nx/2);


NX = ceil((xmax - xmin)/dx);
NY = ceil((ymax - ymin)/dy);
NZ = ceil((zmax - zmin)/dz);

%setup a 3D volume for reconstructed output
recVol = zeros(NX,NY,NZ);	%array to accumulate image data.
recHit = zeros(NX,NY,NZ);	%array to keep track of the number of times data is placed in this element

% downsample the raw image data
sf = res/dx
[ny,nx,nf] = size(hout.data)
for i=1:nf
	scaledImg = imresize(hout.data(:,:,i),sf);
	if i==1
		[sy,sx] = size(scaledImg);
		rawVol = zeros(sy,sx,nf);
	end
	rawVol(:,:,i) = max(0,scaledImg);
end

%convert angles to radians
tmp = hout.yaw;   hout.yaw = tmp * pi/180;
tmp = hout.pitch; hout.pitch = tmp * pi/180;
tmp = hout.roll;  hout.roll = tmp * pi/180;


%Now ready to place the images into recVol
for i=1:nf
	[ Xrt, Yrt, Zrt ] = calc_planeCoords( hout.X(i), hout.Y(i), hout.Z(i), ...
		hout.yaw(i), hout.pitch(i), hout.roll(i), offsetx, offsetz, d1, d2,sx,sy);
    %		0, 0, 0, offsetx, offsetz, d1, d2,sx,sy);
	%convert the planar coordinates to indices
	Xind = round((Xrt - xmin)/dx);
	Yind = round((Yrt - ymin)/dy);
	Zind = round((Zrt - zmin)/dz);

	%test the range of the indices
    maxXind = max(max(Xind));
    maxYind = max(max(Yind));
    maxZind = max(max(Zind));
    minXind = min(min(Xind));
    minYind = min(min(Yind));
    minZind = min(min(Zind));
    
    for iy = 1:sy
		for ix = 1: sx
            xx = Xind(iy,ix);
            yy = Yind(iy,ix);
            zz = Zind(iy,ix);
            
            if (xx > 0) && (xx <= NX)
                if (yy > 0) && (yy <= NY)
                    if (zz > 0) && (zz <= NZ)
                        recVol(xx,yy,zz) = rawVol(iy,ix,i);
                        q=recHit(xx,yy,zz);
                        recHit(xx,yy,zz) = q+1;
                    end
                end
            end
        end
    end
end

%Replace all zeros in recHit with a large number to avoid dividing by zero
list = find( recHit == 0);
recHit(list) = 10000;

%normalize
A = recVol ./ recHit;

%
recVol = imgaussfilt3(A,.5);



