function h = readb8(name,fl)
%
%
%
if nargin < 2, fl=0;end

fp = fopen(name,'rb');
h.type = fread(fp,1,'int');%   int type;    // data type (can be determined by file extensions)
h.frames = fread(fp,1,'int');%    int frames;  // number of frames in file 
h.width = fread(fp,1,'int');%    int w;       // width (number of vectors for raw, image width for processed data)
h.height = fread(fp,1,'int');%    int h;       // height (number of samples for raw, image height for processed data)
h.ss = fread(fp,1,'int');%    int ss;      // data sample size in bits
h.ulx = fread(fp,1,'int');%    int ulx;     // roi - upper left (x) 
h.uly = fread(fp,1,'int');%    int uly;  type   // roi - upper left (y) 
h.urx = fread(fp,1,'int');%    int urx;     // roi - upper right (x) 
h.ury = fread(fp,1,'int');%    int ury;     // roi - upper right (y) 
h.brx = fread(fp,1,'int');%    int brx;     // roi - bottom right (x) 
h.bry = fread(fp,1,'int');%    int bry;     // roi - bottom right (y) 
h.blx = fread(fp,1,'int');%    int blx;     // roi - bottom left (x) 
h.bly = fread(fp,1,'int');%    int bly;     // roi - bottom left (y) 
h.probe = fread(fp,1,'int');%    int probe;   // probe identifier - additional probe information can be found using this id   
h.txf = fread(fp,1,'int');%    int txf;     // transmit frequency in Hz
h.sf = fread(fp,1,'int');%    int sf;      // sampling frequency in Hz
h.dr = fread(fp,1,'int');%    int dr;      // data rate (fps or prp in Doppler modes)
h.ld = fread(fp,1,'int');%    int ld;      // line density (can be used to calculate element spacing if pitch and native # elements is known
h.extra = fread(fp,1,'int');%    int extra;   // extra information (ensemble for color RF)

h.iw = h.urx - h.ulx + 1;
h.ih = h.bry - h.ury + 1;
h.data = zeros(h.iw,h.ih,h.frames);
for i=1:h.frames
    tmp = fread(fp,[h.width,h.height],'uint8');
    A = tmp(h.ulx:h.urx,h.ury:h.bry); %grab the part containing the image
    if fl == 1  
        h.data(:,:,i)=flipud(A);
    elseif fl == 2
        h.data(:,:,i)=fliplr(A);
    else
        h.data(:,:,i)= A;
    end
end

if h.probe == 2     %This is an L14-5/38 probe
    h.res = 38/h.iw
end
fclose(fp);