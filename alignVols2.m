function [outvol,XX,YY,ZZ] = alignVols2( V1, xl1,yl1,zl1,V2, xl2,yl2,zl2)
%this function attempts to perform registration of two volumes.
%reduces the volumes so that only overlapping reion is processed
%

res = 0.001

% Setup the optimizer
[optimizer, metric] = imregconfig('monomodal');
optimizer.MaximumStepLength = 1e-2;
optimizer.MaximumIterations = 200;

% %find the overlap region
% x1list = round([xl1(1):res:xl1(2)]/res);
% x2list = round([xl2(1):res:xl2(2)]/res);
% xlist = intersect(x1list,x2list);
% 
% y1list = round([yl1(1):res:yl1(2)]/res);
% y2list = round([yl2(1):res:yl2(2)]/res);
% ylist = intersect(y1list,y2list);
% 
% z1list = round([zl1(1):res:zl1(2)]/res);
% z2list = round([zl2(1):res:zl2(2)]/res);
% zlist = intersect(z1list,z2list);
% 

xll = max(xl1(1),xl2(1));
xul = min(xl1(2),xl2(2));
yll = max(yl1(1),yl2(1));
yul = min(yl1(2),yl2(2));
zll = max(zl1(1),zl2(1));
zul = min(zl1(2),zl2(2));

%Pick out part of V1 in the overlap region
ix0 = round((xll - xl1(1))/res);
iy0 = round((yll - yl1(1))/res);
iz0 = round((zll - zl1(1))/res);

ix1 = round((xul - xl1(2))/res);
iy1 = round((yul - yl1(2))/res);
iz1 = round((zul - zl1(2))/res);

%Pick out part of V2 in the overlap region
jx0 = round((xll - xl2(1))/res);
jy0 = round((yll - yl2(1))/res);
jz0 = round((zll - zl2(1))/res);

jx1 = round((xul - xl2(2))/res);
jy1 = round((yul - yl2(2))/res);
jz1 = round((zul - zl2(2))/res);

[ny1,nx1,nz1]=size(V1);
[ny2,nx2,nz2]=size(V2);


ttV1 = V1(1+iy0:ny1+iy1,1+ix0:nx1+ix1,1+iz0:nz1+iz1);
ttV2 = V2(1+jy0:ny2+jy1,1+jx0:nx2+jx1,1+jz0:nz2+jz1);

[ny3,nx3,nz3]=size(V2);
center = round(ny3/2);

tV1 = ttV1(center-50:center+50,:,25:end);
tV2 = ttV2(center-50:center+50,:,25:end);




%fixed
 rV1 = imref3d(size(tV1));
% fixed  = pickVolume(V1,xpick, ypick,zpick);
         

 %moving
 rV2 = imref3d(size(tV2));
 
% moving = pickVolume(V2,xpick-xoffset,ypick,zpick);
%     
% %Find transformation to register these images
 tic;
 Tinc =imregtform(tV2, rV2, tV1, rV1, 'rigid', optimizer, metric);
 toc
%     
% %Apply transform to 0,0 to calculate the incremental distance between the
% %images
[XX,YY,ZZ] = transformPointsForward(Tinc,0.0,0.0,0.0);    
%     
%         
% %Apply full transform to the current image to allign it with the previous
%[moved,rmoved] = imwarp(V2,imref3d(size(V2)),Tinc,'linear','outputView',imref3d(size(V1)));
[moved,rmoved] = imwarp(V2,Tinc,'linear','outputView',imref3d(size(V2)));
%[moved,rmoved] = imwarp(V2,imref3d(size(V2)),Tinc,'linear','outputView',rV1);
% 
% 
 outvol = uint8(moved);
%     
% 
% 
