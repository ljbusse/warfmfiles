function [pspec, xc, yc] = calcHistos(data, xsize, ysize, avgrange, stdrange, dflag)
%This is a routine used to identify patches of the image which are likely
%to contains speckle. This determination is based on the average and 
%the standard deviation of values i the patch. Then for each frame grater than 2 and it's
%predecessor r, it calcualtes the correlation 
%coefficient in the patches where both have valid speckle data.
%
%

if nargin < 4; avgrange = [40 100];end
if nargin < 5; stdrange = [4 10];end
if nargin < 6; dflag = 0;end

close all;

[ny,nx,nframes] = size(data);

%center of x,y patches.
xc = 125:xsize:850; 
yc = 100:ysize:650;

lxc = length(xc);
lyc = length(yc);
edges = 0:16:256;
pspec = logical(zeros(lyc,lxc,nframes));
average = zeros(lyc,lxc);
standev = zeros(lyc,lxc);
corcoef = zeros(lyc,lxc,nframes);
xll = fix(xsize/2);
yll = fix(ysize/2);
A = zeros(1,nframes);
B = zeros(1,nframes);
C = zeros(1,nframes);


if dflag
    gs = colormap(gray(256));
    df = colormap(jet(16));
end

for k=1:nframes
    tmp = data(:,:,k);
    for i=1:lyc    
        for j= 1:lxc
            patch = tmp(yc(i)-yll:yc(i)+yll, xc(j)-xll:xc(j)+xll);
            vpatch = reshape(patch,[xsize*ysize,1]);
            average(i,j) = mean(vpatch);
            standev(i,j) = std(single(vpatch));
    %        histos(i,j,:) = histcounts(patch,edges);
        end
    end

    s1 = zeros(lyc,lxc);
    s2 = zeros(ny,nx);
    s3 = logical(zeros(lyc,lxc));

    l1 = find( standev > stdrange(1) & standev <= stdrange(2));
    l2 = find( average > avgrange(1) & average <= avgrange(2));

    lc = intersect(l1,l2);

    s1(lc) = standev(lc);
    s3(lc) = 1;
    pspec(:,:,k) = logical(s3);%s1;
    if dflag
        qq=sprintf('Frame %d',k);
        f1=figure(1); imagesc(average,avgrange);title(['Average: ' qq]);colormap(gs);

        f2=figure(2); imagesc(standev,stdrange);title(['StdDev: ' qq]);colormap(df);
        set(f2,'Position',[635 528 560 420]);
        f3=figure(3); image(data(:,:,k)); colormap(gs);title(['Image: ' qq]);
        set(f3,'Position',[1199 527 560 420]);
        f4=figure(4); image(s1);title(['Speckle: ' qq]);colormap(df);
        set(f4,'Position',[596 11 570 420]);
        
        if k ==1
            set(f1,'Position',[65 528 570 420]);
            set(f2,'Position',[635 528 560 420]);
            set(f3,'Position',[1199 527 560 420]);
            set(f4,'Position',[596 11 570 420]);
        end
        

    %     figure(4);
    %         l3 = find(tmp > 190);
    %         s2(l3) = tmp(l3);
    %         image(s2);colormap(gs);title(qq);


        drawnow;
    end
%     min(min(standev))
%     max(max(standev))
    if k > 1    %try doing the de-correlation analysis
        valid = and(pspec(:,:,k-1), pspec(:,:,k));   %identify patches in this frame and previous frame that might contain speckle
        X = zeros(lyc,lxc);
        Y = zeros(lyc,lxc);
        Z = zeros(lyc,lxc);
        for i=1:lyc    
            for j= 1:lxc
                if valid(i,j)
                    patch     = tmp(yc(i)-yll:yc(i)+yll, xc(j)-xll:xc(j)+xll);
                    prevpatch = prevtmp(yc(i)-yll:yc(i)+yll, xc(j)-xll:xc(j)+xll);
                    %calculate the correlation coef
                    corcoef(i,j,k) = corr2(patch,prevpatch);
                    X(i,j) = xc(j);
                    Y(i,j) = yc(i);
                    Z(i,j) = 2 * (1 - corcoef(i,j,k)); %convert corcoef to distance
        
                end
            end
        end
        %lets have a look
        f10=figure(10);
        if k == 2, set(f10,'Position',[1190 6 560 420]);end
        
        imagesc(corcoef(:,:,k),[0 1]);
        colormap(gray);
        qq=sprintf('Frame %d',k);
        title(['decorr values: ' qq]);
        
        pause(0.2);
        
        list = find( X ~= 0 );
        XX(list) = X(list);
        YY(list) = Y(list);
        ZZ(list) = Z(list);
       
        %Fit the plane to estimate probe motion
        [A(k),B(k),C(k)]=plane_fit(XX(:),YY(:),ZZ(:))
        
       

    end
    prevtmp = tmp;
%    pause;

end

figure(14);
plot(cumsum(atan(A)*180/pi));
grid;
xlabel('Sample Number');
ylabel('Angle');
title('Yaw Plane fit');

figure(15);
plot(cumsum(atan(B)*180/pi));
grid;
xlabel('Sample Number');
ylabel('Angle');
title('Roll Plane fit');

figure(16);plot(cumsum(C))
grid;
xlabel('Sample Number');
ylabel('Distance');
title('proportional to displacement');



end

