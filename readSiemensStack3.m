function hout = readSiemensStack3( ufilename,displayflag)
%
%
displayflag = 0;
if nargin < 2, displayflag =1; end

%open ultrasound file and read all
h=readInfo(ufilename);

% %open tracker file and match the time to the frames
% fid = fopen(h.tname);
% 
% %number of frames to be used
% numFrames = h.nframes - frame0;%+1;
% timeinc = 1./h.dr;

%allocate some space for output data
hout.X = zeros(numFrames,1);
hout.Y = zeros(numFrames,1);
hout.Z = zeros(numFrames,1);
hout.yaw = zeros(numFrames,1);
hout.pitch = zeros(numFrames,1);
hout.roll = zeros(numFrames,1);
hout.data = uint8(zeros(h.ih,h.iw,numFrames));
hout.res = h.res
hout.xoffset = h.xoffset;
hout.yoffset = h.yoffset;
hout.zoffset = h.zoffset;
hout.tindex = zeros(numFrames,1);

% Read data from csv file
% readline = fgetl(fid);
count = 1;
index = 1;
% frame = frame0;
% timetarget = t0;

hh = readIMA(ufilename,h.flip,'info.csv',displayflag); %try no left/right flip

while readline ~= -1
    [A,COUNT,ERRMSG,NEXTINDEX] = sscanf(readline,'%d,%d,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%f');
    time = A(9);
    if time < timetarget
        count = count +1;
    else
        if index <= numFrames
            hout.X(index)= A(3);
            hout.Y(index)= A(4); %-
            hout.Z(index)= A(5);
            hout.yaw(index)= A(6);%*pi/180;  -
            hout.pitch(index)= A(7);%*pi/180;
            hout.roll(index)= A(8);%*pi/180; -
    %        index
    %        frame
    %        count
            hout.data(:,:,index) = uint8(hh.data(:,:,frame));
            hout.tindex(index) = count;
            index = index +1;
            frame = frame +1;
        end
        count = count +1;
        timetarget = timetarget + timeinc;
    end
    readline = fgetl(fid);
end
fclose(fid);
