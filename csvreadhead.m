function x = csvreadhead(filename,numLines)
%Read the first few lines of a CSV file without having to load and parse the
%entire file. It's expected that the CSV file contains exclusively numeric
%data.
%
%Mike Thiem, Oct 2 2017
%mike.j.thiem@gmail.com
%
%Larry Busse, Sept 21, 2018
%ljbusse@protonmail.me


%Read the specified number of lines from the end of the CSV file
s = head(filename,numLines);
if s(end) == char(10)
    s(end) = [];
end

%Convert the string to a numeric matrix
x = parsecsv(s);

if all(isnan(x(end,:)))
    %The last line was mssing some columns, so omit it
    x(end,:) = [];
end