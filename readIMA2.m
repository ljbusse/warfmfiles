function h = readIMA2(name,fl,mf)
%
%
%

if nargin < 2, fl=0;end %flip flag, defualt = no
if nargin < 3, mf=1;end %Mask flag, default = yes

data = dicomread(name);
[ny,nx,nb,nframes] = size(data);

h.data = uint8(zeros(ny,nx,nframes));
h.frames = nframes;

load ../mask;

for i=1:h.frames
%     tmp = uint8(data(:,:,1,i) .* m0);
%     A = tmp(h.ury:h.bry,h.ulx:h.urx); %grab the part containing the image
    A= uint8(data(:,:,1,i) .* m0);
    if mf == 1
            A= uint8(data(:,:,1,i) .* m0);
    else
            A= uint8(data(:,:,1,i));
    end
    
    if fl == 1  
        h.data(:,:,i)=flipud(A);
    elseif fl == 2
        h.data(:,:,i)=fliplr(A);
    else
        h.data(:,:,i)= A;
    end
end



