%[data,xres,yres]=readUltrasonixStack('.');
%h=readb8('l1.b8');
h=readb8('short-pan.b8');

[optimizer, metric] = imregconfig('monomodal');
optimizer.MaximumStepLength = 1e-2;
optimizer.MaximumIterations = 100;


[ysizeorg,xsizeorg,nslices]=size(h.data);



ycenter = fix(ysizeorg/2);
xcenter = fix(xsizeorg/2);

%width of the strips used for image registration
xwidth = 40;
ywidth = 300

%put F1 in Final

Final = zeros(1000, 3000);
ftemp = zeros(1000, 3000);
testI = zeros(99,99);
testI(50,50) = 1;

F1 = squeeze(h.data(:,:,1));    
fixed  = pickstrip(F1', xwidth, 0);
% is the first image...put it all in Final
Final(500-xcenter:500+xcenter,500-ycenter:500+ycenter) = F1';
deltax = 0;
deltay = 0;
xstart = 500-xcenter;
ystart = 500-ycenter;

xo2 = round(xsizeorg/2);
yo2 = round(ysizeorg/2);
for k=2:nslices-2
    ftemp = Final;
    F1 = squeeze(h.data(:,:,k-1));
    fixed  = pickstrip(F1', xwidth, 1);
    F2 = squeeze(h.data(:,:,k));
    moving   = pickstrip(F2', xwidth, 2);

    tform = imregtform(moving, fixed,'rigid', optimizer, metric);
    
    %try applying the transform to learn translation and rotation
    [t1,t2] = transformPointsForward(tform,0,0);
    [r1,r2] = transformPointsForward(tform,1,1);

    %Temp = imwarp(testI,tform,'OutputView',imref2d(size(testI)));
    %[ypeak, xpeak] = find(Temp==max(Temp(:)))
    xpeak = round(t1);
    ypeak = round(t2);
    
    
    
%    figure(3);imagesc(Temp); colormap(gray); colorbar; axis image; %drawnow;
    
    moved = imwarp(F2',tform,'OutputView',imref2d(size(F1')));
%    moved = imwarp(F2',tform,'OutputView',imref2d(size(Final)));
%    figure(4);imagesc(moved); colormap(gray); colorbar; axis image; %drawnow;
    
    %deltay = deltay + ypeak-50;
    %deltax = deltax + xpeak-50;
    deltay = deltay + ypeak;
    deltax = deltax + xpeak;
    
    %Adjust the right half of Final
    x1 = xstart + deltay;
    y1 = yo2 + ystart + deltax;

    %the right hal,f of moved
    qq=moved(:,yo2:end);
    
    Final(x1:x1+xsizeorg-1, y1:y1+yo2-1) = qq;
    
    figure(14);imagesc(Final); colormap(gray); colorbar; axis image; drawnow;
end


