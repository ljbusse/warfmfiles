function test_transform_logfile( filename)

%% Input Values
offsetx=0.0447;
offsetz=0.0130;
%offsetx = .003; offsetz = -0.006;      % Offset distance of sensor in x and z directions (in m)

d1=0.038; d2 = 0.1;                    % Length and width of the rectangular Image (in meter)

%This is a fake ultrasound image
I = ones(128,300);
II= cumsum(I,2);
II(:,1) = II(:,300);



%% Open data file
fid = fopen(filename);

% Read data from csv file
readline = fgetl(fid);
count = 1;
while readline ~= -1
%    [A,COUNT,ERRMSG,NEXTINDEX] = sscanf(readline,'%d,%d,%f,%f,%f,%f,%f,%f,%f,%d,%d,%d,%f');
     [A,COUNT,ERRMSG,NEXTINDEX] = sscanf(readline,'%f %f %f %f %f %f %f');
    X=A(1)/100;
    Y=A(2)/100;
    Z=A(3)/100;
    yaw=A(4)*pi/180;
    pitch=A(5)*pi/180;
    roll=A(6)*pi/180;
    time = A(7);
%    if mod(count,10) == 0
        
        [ Xabcd, Yabcd, Zabcd ] = calc_corners( X, Y, Z, yaw, pitch, roll,...
            offsetx, offsetz, d1, d2 );
        
        figure(10);
        scatter3(Xabcd,Yabcd,Zabcd,[10,20,30,40]);hold on;
        scatter3(X,Y,Z,'x');
        drawnow;
        
%        hold off;
        
        xc = [Xabcd(1) Xabcd(2); Xabcd(4) Xabcd(3)];
        yc = [Yabcd(1) Yabcd(2); Yabcd(4) Yabcd(3)];
        zc = [Zabcd(1) Zabcd(2); Zabcd(4) Zabcd(3)];

        S=surface('XData',xc,...
       'YData',yc,...
       'ZData',zc,...
       'CData',II,...
       'FaceColor','texturemap');
%    end

    count = count +1;
    readline = fgetl(fid);
end
xlabel('x-coordinate');
ylabel('y-coordinate');
zlabel('z-coordinate');
%axis([-.5,.5,-.5,.5,.5,1.5]);
axis image;
fclose(fid);

