function [recVol,xl,yl,zl,hout] = recVolume3_siemens( ufilename,dx,align)
%this program is used to reconstruct 3D volume images based on tracker data only.
%Input arguments are the tracker filename, ultrasound filename and the
%final resolution desired.
%outputs the reconstructed volume, and the limits in X, Y, and Z.
%optionally output the raw data.

%set the resolution
if nargin  < 2, dx = 0.0005;end
if nargin < 3, align = 0; end

dy = dx;	%make resolution uniform in all directions
dz = dx;	

%read all the frames and coordinates
[f0,t0,tt,ut] = calc_times_siemens(ufilename);
displayflag = 0;
hout = syncFrameTimes_Siemens( t0, f0, ufilename,displayflag);

%hout = smoothTrackerData( hout,21);
%hout = zeroData(hout);

if align == 1
%    [stack,XX,YY]=alignStack(hout.data);
    [stack,XX,YY]=alignInPlane(hout.data);
    hout.data = stack;
%     %Assumes array parallel to X
%     X0=hout.X(1);
%     hout.X = X0 + cumsum(XX) * dx;
% %     %Assumes array parallel to Y 
% %     Y0=hout.Y(1);
% %     hout.Y = Y0 + cumsum(XX) * dy;
% %     
% %     Z0=hout.Z(1);
% %     hout.Z = Z0 + cumsum(YY) * dz;  
    
end

%boresight set with probe vertical and || to Y tracker axis
offsetx=hout.xoffset/1000;%-0.015;     	%boresight set with probe vertical and || to Y tracker axis
offsety=hout.yoffset/1000;              %boresight set with probe vertical and || to X tracker axis
offsetz=hout.zoffset/1000;%0.040;


%adjust to accomodate the US image
[ny,nx,nf] = size(hout.data);

res = hout.res/1000;
d1 = res*nx;            	%width of image (meters)
d2 = res*ny;                %adjust the depth to the FOV used.

%determine the overall extent of the volume to be reconstructed (in tracker coordinates)
%boresight set with probe vertical and || to X tracker axis
xmin = min(hout.X) %- d1/2;
ymin = min(hout.Y) - d1/2;%+ offsety;
zmin = min(hout.Z) + offsetz;
xmax = max(hout.X) %+ d1/2;
ymax = max(hout.Y) + d1/2;%+ offsety;
zmax = max(hout.Z) + offsetz;

zmin = zmin-0.01;% + 0.005;     %try to eliminate the skin=line
zmax = zmax + (res * ny);   %Add a bit to accomodate the US image
ymin = ymin -.010;
ymax = ymax +.010;
% xmin = xmin - (res * nx/2)-.010;
% xmax = xmax + (res * nx/2)+.010;

NX = ceil((xmax - xmin)/dx);
NY = ceil((ymax - ymin)/dy);
NZ = ceil((zmax - zmin)/dz);

%setup a 3D volume for reconstructed output
recVol = uint8(zeros(NY,NX,NZ));   %array to accumulate image data.
recHit = uint8(zeros(NY,NX,NZ));   %array to keep track of the number of times data is placed in this element

% downsample the raw image data
sf = res/dx
[ny,nx,nf] = size(hout.data)
for i=1:nf
	scaledImg = imresize(hout.data(:,:,i),sf);
	if i==1
		[sy,sx] = size(scaledImg);
		rawVol = zeros(sy,sx,nf);
	end
	rawVol(:,:,i) = max(0,scaledImg);
end

%convert angles to radians
tmp = hout.yaw;   hout.yaw = tmp * pi/180;
tmp = hout.pitch; hout.pitch = tmp * pi/180;
tmp = hout.roll;  hout.roll = tmp * pi/180;


%Now ready to place the images into recVol
for i=1:nf
	[ Xrt, Yrt, Zrt ] = calc_planeCoords2( hout.X(i), hout.Y(i), hout.Z(i), ...
   		hout.yaw(i), hout.pitch(i), hout.roll(i), offsety, offsetz, d1, d2,sx,sy);
%      		0, 0, 0, offsety, offsetz, d1, d2,sx,sy);

	%convert the planar coordinates to indices
	Xind = round((Xrt - xmin)/dx);
	Yind = round((Yrt - ymin)/dy);
	Zind = round((Zrt - zmin)/dz);

	%test the range of the indices
%     maxXind = max(max(Xind));
%     maxYind = max(max(Yind));
%     maxZind = max(max(Zind));
%     minXind = min(min(Xind));
%     minYind = min(min(Yind));
%     minZind = min(min(Zind));
    
    for iy = 1:sy
		for ix = 1: sx
            xx = Xind(iy,ix);
            yy = Yind(iy,ix);
            zz = Zind(iy,ix);
            
            if (xx > 0) && (xx <= NX)
                if (yy > 0) && (yy <= NY)
                    if (zz > 0) && (zz <= NZ)
                        v = recVol(yy,xx,zz);       %fetch the current value
                        q = recHit(yy,xx,zz);       %fetch number of hits
                        vs = double(v) * double(q); %scale UP
                        vs = vs + rawVol(iy,ix,i);    %add new value
                        q = q+1;                    %Add 1 to the hits
                        recVol(yy,xx,zz) = uint8(vs/q); %scale DOWN and store
                        recHit(yy,xx,zz) = q;       %store hits
                    end
                end
            end
        end
    end
end


%normalize
%A = recVol ./ recHit;

%%do some hole filling
% recIx = holefillxint(recVol);
% recIy = holefillyint(recVol);
% recVol = recIx/2 + recIy/2;
xl = [xmin,xmax];
yl = [ymin,ymax];
zl = [zmin,zmax];



